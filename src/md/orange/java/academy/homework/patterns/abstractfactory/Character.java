package md.orange.java.academy.homework.patterns.abstractfactory;

public interface Character {
    void attack();
}
