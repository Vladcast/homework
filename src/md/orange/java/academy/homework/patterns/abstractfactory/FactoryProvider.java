package md.orange.java.academy.homework.patterns.abstractfactory;

import md.orange.java.academy.homework.patterns.abstractfactory.factory.EnemyFactory;
import md.orange.java.academy.homework.patterns.abstractfactory.factory.AbstractFactory;
import md.orange.java.academy.homework.patterns.abstractfactory.factory.AllyFactory;

public class FactoryProvider {

    public static AbstractFactory getFactory(String character) {

        if (character.equalsIgnoreCase("Ally")) {
            return new AllyFactory();

        } else if (character.equalsIgnoreCase("Enemy")) {
            return new EnemyFactory();
        }

        return null;
    }

}