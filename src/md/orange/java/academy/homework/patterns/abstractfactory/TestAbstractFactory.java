package md.orange.java.academy.homework.patterns.abstractfactory;

import md.orange.java.academy.homework.patterns.abstractfactory.ally.Ally;
import md.orange.java.academy.homework.patterns.abstractfactory.enemy.Enemy;
import md.orange.java.academy.homework.patterns.abstractfactory.factory.AbstractFactory;

public class TestAbstractFactory {
    public static void main(String[] args) {
        AbstractFactory allyFactory = FactoryProvider.getFactory("Ally");
        Ally allySoldier = allyFactory.getAlly("Ally Soldier");
        allySoldier.attack();

        AbstractFactory enemyFactory = FactoryProvider.getFactory("Enemy");
        Enemy enemyWitch = enemyFactory.getEnemy("Enemy Witch");
        enemyWitch.attack();
    }
}
