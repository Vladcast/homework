package md.orange.java.academy.homework.patterns.abstractfactory.ally;

import md.orange.java.academy.homework.patterns.abstractfactory.Character;

public interface Ally extends Character {
}
