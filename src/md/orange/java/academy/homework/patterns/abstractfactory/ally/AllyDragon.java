package md.orange.java.academy.homework.patterns.abstractfactory.ally;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;

public class AllyDragon  implements Flyable, Ally {

    public void fly() {
        System.out.println("I can Fly!");
    }

    @Override
    public void attack() {

    }
}
