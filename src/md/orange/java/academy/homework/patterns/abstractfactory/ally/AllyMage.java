package md.orange.java.academy.homework.patterns.abstractfactory.ally;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;

public class AllyMage   implements Ally,Flyable {

    public void castSpell() {
        System.out.println(this.getClass().getSimpleName() + " can Cast Spell");
    }

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " can Fly");
    }

    @Override
    public void attack() {

    }
}
