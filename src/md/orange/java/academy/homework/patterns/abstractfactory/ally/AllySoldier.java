package md.orange.java.academy.homework.patterns.abstractfactory.ally;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Walkable;

public class AllySoldier implements Ally, Walkable {

    public void walk() {
        System.out.println(this.getClass().getSimpleName() + " can Walk!");
    }

    public void protect() {
        System.out.println(this.getClass().getSimpleName() + " can Protect!");
    }

    @Override
    public void attack() {
        System.out.println(this.getClass().getSimpleName() + " attack like a Soldier can");
    }
}
