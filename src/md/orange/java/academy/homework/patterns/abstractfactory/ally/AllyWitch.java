package md.orange.java.academy.homework.patterns.abstractfactory.ally;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;
import md.orange.java.academy.homework.patterns.abstractfactory.skills.Walkable;

public class AllyWitch implements Ally,Flyable, Walkable {

    @Override
    public void fly() {
        System.out.println("I can fly");
    }

    @Override
    public void walk() {
        System.out.println("I can walk");
    }

    public void hide() {
        System.out.println("I can hide");
    }

    @Override
    public void attack() {

    }
}
