package md.orange.java.academy.homework.patterns.abstractfactory.enemy;

import md.orange.java.academy.homework.patterns.abstractfactory.Character;

public interface Enemy extends Character {
}
