package md.orange.java.academy.homework.patterns.abstractfactory.enemy;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Commander;
import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;

public class EnemyCommander implements Enemy, Flyable, Commander {

    @Override
    public void promoteSubordinate() {
        System.out.println(this.getClass().getSimpleName() + " can Promote Subordinate!");
    }

    @Override
    public void sendOrders() {
        System.out.println(this.getClass().getSimpleName() + " can Send Orders!");
    }

    @Override
    public void fly() {
        System.out.println("Fly");
    }

    @Override
    public void attack() {

    }
}
