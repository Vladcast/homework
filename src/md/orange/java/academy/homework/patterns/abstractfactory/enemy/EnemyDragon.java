package md.orange.java.academy.homework.patterns.abstractfactory.enemy;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;

public class EnemyDragon implements Enemy, Flyable {

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " can Fly");
    }

    @Override
    public void attack() {

    }
}
