package md.orange.java.academy.homework.patterns.abstractfactory.enemy;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Walkable;

public class EnemySoldier implements Enemy, Walkable {
    @Override
    public void walk() {
        System.out.println(this.getClass().getSimpleName() + " can Walk!");
    }

    @Override
    public void attack() {

    }
}
