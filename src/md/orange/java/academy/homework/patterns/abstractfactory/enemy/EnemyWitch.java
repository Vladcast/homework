package md.orange.java.academy.homework.patterns.abstractfactory.enemy;

import md.orange.java.academy.homework.patterns.abstractfactory.skills.Flyable;

public class EnemyWitch implements Enemy, Flyable {

    public void curse() {
        System.out.println(this.getClass().getSimpleName() + " can Curse");
    }

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " can Fly");
    }

    @Override
    public void attack() {
        System.out.println(this.getClass().getSimpleName() + " attack like Witch can");
    }
}
