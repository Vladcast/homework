package md.orange.java.academy.homework.patterns.abstractfactory.factory;

import md.orange.java.academy.homework.patterns.abstractfactory.enemy.Enemy;
import md.orange.java.academy.homework.patterns.abstractfactory.ally.Ally;

public abstract class AbstractFactory {

    public abstract Ally getAlly(String allyType);

    public abstract Enemy getEnemy(String enemyType);
}
