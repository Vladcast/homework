package md.orange.java.academy.homework.patterns.abstractfactory.factory;

import md.orange.java.academy.homework.patterns.abstractfactory.ally.*;
import md.orange.java.academy.homework.patterns.abstractfactory.enemy.Enemy;

public class AllyFactory extends AbstractFactory {

    @Override
    public Ally getAlly(String allyType) {
        if (allyType == null) {
            return null;
        }
        if (allyType.equalsIgnoreCase("Ally Soldier")) {
            return new AllySoldier();
        }
        if (allyType.equalsIgnoreCase("Ally Dragon")) {
            return new AllyDragon();
        }
        if (allyType.equalsIgnoreCase("Ally Commander")) {
            return new AllyCommander();
        }
        if (allyType.equalsIgnoreCase("Ally Witch")) {
            return new AllyWitch();
        }
        if (allyType.equalsIgnoreCase("Ally Mage")) {
            return new AllyMage();
        }

        return null;
    }

    @Override
    public Enemy getEnemy(String enemyType) {
        return null;
    }
}
