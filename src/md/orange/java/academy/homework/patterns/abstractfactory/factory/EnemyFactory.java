package md.orange.java.academy.homework.patterns.abstractfactory.factory;

import md.orange.java.academy.homework.patterns.abstractfactory.ally.Ally;
import md.orange.java.academy.homework.patterns.abstractfactory.enemy.*;

public class EnemyFactory extends AbstractFactory {

    @Override
    public Ally getAlly(String personajType) {
        return null;
    }

    @Override
    public Enemy getEnemy(String personajType) {
        if (personajType == null) {
            return null;
        }
        if (personajType.equalsIgnoreCase("Enemy Soldier")) {
            return new EnemySoldier();
        }
        if (personajType.equalsIgnoreCase("Enemy Witch")) {
            return new EnemyWitch();
        }
        if (personajType.equalsIgnoreCase("Enemy Commander")) {
            return new EnemyCommander();
        }
        if (personajType.equalsIgnoreCase("Enemy Dragon")) {
            return new EnemyDragon();
        }
        return null;
    }
}
