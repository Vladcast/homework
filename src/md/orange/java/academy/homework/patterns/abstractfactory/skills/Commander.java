package md.orange.java.academy.homework.patterns.abstractfactory.skills;

public interface Commander {

     void promoteSubordinate();

     void sendOrders();
}
