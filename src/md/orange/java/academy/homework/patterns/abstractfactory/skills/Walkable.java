package md.orange.java.academy.homework.patterns.abstractfactory.skills;

public interface Walkable {
    void walk();
}
