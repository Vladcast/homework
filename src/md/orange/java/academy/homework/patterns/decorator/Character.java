package md.orange.java.academy.homework.patterns.decorator;

public interface Character {

    String attack();
}
