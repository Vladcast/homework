package md.orange.java.academy.homework.patterns.decorator;

public class CharacterImpl implements Character {

    @Override
    public String attack() {
        return " attack ";
    }
}
