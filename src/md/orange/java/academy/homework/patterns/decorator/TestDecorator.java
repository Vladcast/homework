package md.orange.java.academy.homework.patterns.decorator;

import md.orange.java.academy.homework.patterns.decorator.ally.AllyCommander;
import md.orange.java.academy.homework.patterns.decorator.ally.AllyMage;

public class TestDecorator {

    public static void main(String[] args) {
        Character allyCommander = new AllyMage(new AllyCommander(new CharacterImpl()));
        System.out.println(allyCommander.attack());

    }
}
