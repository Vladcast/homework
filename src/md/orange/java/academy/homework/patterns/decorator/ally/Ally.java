package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.Character;

public class Ally implements Character {

    protected Character character;

    public Ally(Character p) {
        this.character = p;
    }

    @Override
    public String attack() {
        return this.character.attack();
    }
}
