package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.Character;
import md.orange.java.academy.homework.patterns.decorator.skills.Commander;
import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;

public class AllyCommander extends Ally implements Flyable, Commander {

    public AllyCommander(Character character) {
        super(character);
    }

    @Override
    public String sendOrders() {
        return "and Send Orders ";
    }

    @Override
    public String promoteSubordinate() {
        return "and Promote Subordinate ";
    }

    @Override
    public String fly() {
        return "and Fly ";
    }

    @Override
    public String attack() {
        return super.attack() + sendOrders() + fly() + promoteSubordinate() + "like a Commander ";
    }
}
