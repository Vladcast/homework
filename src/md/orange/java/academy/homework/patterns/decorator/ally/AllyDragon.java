package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;
import md.orange.java.academy.homework.patterns.decorator.Character;

public class AllyDragon extends Ally implements Flyable {

    public AllyDragon(Character character) {
        super(character);
    }

    public String fly() {
        return "I can Fly!";
    }

    @Override
    public String attack() {
        return super.attack();
    }
}
