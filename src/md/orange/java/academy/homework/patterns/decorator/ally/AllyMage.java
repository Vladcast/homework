package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;
import md.orange.java.academy.homework.patterns.decorator.Character;

public class AllyMage extends Ally implements Flyable {

    public AllyMage(Character character) {
        super(character);
    }

    public String castSpell() {
        return this.getClass().getSimpleName() + " and Cast Spell";
    }

    public String fly() {
        return this.getClass().getSimpleName() + " and Fly";
    }

    @Override
    public String attack() {
        return getClass().getSimpleName() + " " + super.attack();
    }
}
