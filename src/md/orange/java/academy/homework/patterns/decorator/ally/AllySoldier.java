package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.Character;
import md.orange.java.academy.homework.patterns.decorator.skills.Walkable;

public class AllySoldier extends Ally implements Walkable {

    public AllySoldier(Character character) {
        super(character);
    }

    public String walk() {
        return this.getClass().getSimpleName() + " and Walk!";
    }

    public String protect() {
        return this.getClass().getSimpleName() + " and Protect!";
    }

    @Override
    public String attack() {
        return super.attack();
    }
}
