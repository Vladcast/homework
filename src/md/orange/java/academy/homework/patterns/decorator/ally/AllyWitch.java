package md.orange.java.academy.homework.patterns.decorator.ally;

import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;
import md.orange.java.academy.homework.patterns.decorator.Character;
import md.orange.java.academy.homework.patterns.decorator.skills.Walkable;

public class AllyWitch extends Ally implements Flyable, Walkable {

    public AllyWitch(Character character) {
        super(character);
    }

    @Override
    public String fly() {
        return " and fly";
    }

    @Override
    public String walk() {
        return "and walk";
    }

    public String hide() {
        return "and hide";
    }

    @Override
    public String attack() {
        return super.attack();
    }
}
