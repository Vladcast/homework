package md.orange.java.academy.homework.patterns.decorator.enemy;

import md.orange.java.academy.homework.patterns.decorator.Character;

public abstract class Enemy implements Character {
    protected Character character;

    public Enemy(Character character) {
        this.character = character;
    }

    @Override
    public String attack() {
        return this.getClass().getSimpleName() + " can attack like Enemy";
    }
}
