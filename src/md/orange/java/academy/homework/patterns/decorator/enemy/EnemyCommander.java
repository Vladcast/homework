package md.orange.java.academy.homework.patterns.decorator.enemy;

import md.orange.java.academy.homework.patterns.decorator.skills.Commander;
import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;
import md.orange.java.academy.homework.patterns.decorator.Character;

public class EnemyCommander extends Enemy implements Flyable, Commander {

    public EnemyCommander(Character character) {
        super(character);
    }

    @Override
    public String promoteSubordinate() {
        return this.getClass().getSimpleName() + " and Promote Subordinate!";
    }

    @Override
    public String sendOrders() {
        return this.getClass().getSimpleName() + " and Send Orders!";
    }

    @Override
    public String fly() {
        return "Fly";
    }

    @Override
    public String attack() {
        return super.attack();
    }
}
