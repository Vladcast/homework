package md.orange.java.academy.homework.patterns.decorator.enemy;

import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;
import md.orange.java.academy.homework.patterns.decorator.Character;

public class EnemyDragon extends Enemy implements Flyable {

    public EnemyDragon(Character character) {
        super(character);
    }

    public String fly() {
        return this.getClass().getSimpleName() + " can Fly";
    }

    @Override
    public String attack() {
        return super.attack();
    }
}

