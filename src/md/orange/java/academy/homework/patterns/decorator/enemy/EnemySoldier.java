package md.orange.java.academy.homework.patterns.decorator.enemy;

import md.orange.java.academy.homework.patterns.decorator.Character;
import md.orange.java.academy.homework.patterns.decorator.skills.Walkable;

public class EnemySoldier extends Enemy implements Walkable {
    public EnemySoldier(Character character) {
        super(character);
    }

    @Override
    public String attack() {
        return super.attack();
    }

    @Override
    public String walk() {
        return this.getClass().getSimpleName() + " can Walk!";
    }
}

