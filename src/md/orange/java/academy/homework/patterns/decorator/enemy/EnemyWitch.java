package md.orange.java.academy.homework.patterns.decorator.enemy;

import md.orange.java.academy.homework.patterns.decorator.Character;
import md.orange.java.academy.homework.patterns.decorator.skills.Flyable;

public class EnemyWitch extends Enemy implements Flyable {

    public EnemyWitch(Character character) {
        super(character);
    }

    public String curse() {
        return this.getClass().getSimpleName() + " and Curse";
    }

    public String fly() {
        return this.getClass().getSimpleName() + " and Fly";
    }

    @Override
    public String attack() {
        return character.attack();
    }
}
