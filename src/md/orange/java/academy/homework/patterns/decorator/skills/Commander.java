package md.orange.java.academy.homework.patterns.decorator.skills;

public interface Commander {

    String promoteSubordinate();

    String sendOrders();
}
