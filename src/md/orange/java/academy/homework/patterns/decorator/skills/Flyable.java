package md.orange.java.academy.homework.patterns.decorator.skills;

public interface Flyable {

    String fly();
}
