package md.orange.java.academy.homework.patterns.decorator.skills;

public interface Walkable {

    String walk();
}
