package md.orange.java.academy.homework.patterns.mediator;

public abstract class Enemy {
    protected EnemyMediator enemyMediator;
    protected String name;

    public Enemy(EnemyMediator enemyMediator, String name) {
        this.enemyMediator = enemyMediator;
        this.name = name;
    }

    public abstract void send(String command);

    public abstract void receive(String command);

}
