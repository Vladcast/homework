package md.orange.java.academy.homework.patterns.mediator;

public class EnemyImpl extends Enemy {
    public EnemyImpl(EnemyMediator enemyMediator, String name) {
        super(enemyMediator, name);
    }

    @Override
    public void send(String command) {
        System.out.println(this.name + ": Sending Command = " + command);
        enemyMediator.sendCommand(command, this);
    }

    @Override
    public void receive(String command) {
        System.out.println(this.name + ": Received Command = " + command);
    }
}
