package md.orange.java.academy.homework.patterns.mediator;

interface EnemyMediator {

    void sendCommand(String command, Enemy enemy);

    void addEnemy(Enemy enemy);

    }
