package md.orange.java.academy.homework.patterns.mediator;

import java.util.ArrayList;
import java.util.List;

public class EnemyMediatorImpl implements EnemyMediator {

    private List<Enemy> enemies;

    public EnemyMediatorImpl() {
        this.enemies = new ArrayList<>();
    }

    @Override
    public void addEnemy(Enemy enemy) {
        this.enemies.add(enemy);
    }

    @Override
    public void sendCommand(String command, Enemy enemy) {
        enemies.stream()
                .filter(e -> e != enemy)
                .forEach(e -> e.receive(command));
    }
}
