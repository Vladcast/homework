package md.orange.java.academy.homework.patterns.mediator;

public class MediatorTest {
    public static void main(String[] args) {
        EnemyMediator enemyMediator = new EnemyMediatorImpl();
        Enemy commander = new EnemyImpl(enemyMediator, "Ally Commander");
        Enemy dragon = new EnemyImpl(enemyMediator, "Ally Dragon");
        Enemy soldier = new EnemyImpl(enemyMediator, "Ally Soldier");
        Enemy witch = new EnemyImpl(enemyMediator, "Ally Witch");

        enemyMediator.addEnemy(commander);
        enemyMediator.addEnemy(dragon);
        enemyMediator.addEnemy(soldier);
        enemyMediator.addEnemy(witch);

        commander.send("Attack!");
    }
}
