package md.orange.java.academy.homework.patterns.singleton;

import md.orange.java.academy.homework.solid.solidV1.SkillView;

public class AllyCommander {
    private static AllyCommander allyCommanderIntance = new AllyCommander();

    private AllyCommander() {
    }

    public static AllyCommander getAllyCommanderIntance() {
        return allyCommanderIntance;
    }

    public String showSkills() {
        return "Ally Commander can attack, send orders and promote subordinates";
    }

}
