package md.orange.java.academy.homework.patterns.singleton;

public class SingletonTest {
    public static void main(String[] args) {
        AllyCommander allyCommander = AllyCommander.getAllyCommanderIntance();
        System.out.println(allyCommander.showSkills());
    }
}
