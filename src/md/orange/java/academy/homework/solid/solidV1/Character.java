package md.orange.java.academy.homework.solid.solidV1;

import java.util.ArrayList;

public class Character extends SkillView{
    private CharacterEnum character;
    private String nickName;
    private Long level;
    ArrayList<SkillEnumeration> skill;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public CharacterEnum getRace() {
        return character;
    }

    public void setRace(CharacterEnum character) {
        this.character = character;
    }

    public ArrayList<SkillEnumeration> getSkill() {
        return skill;
    }

    public void setSkill() {
        this.skill = new SkillSetterImpl().setSkill(this);
    }

    public Character(CharacterEnum character, String nickName, Long level) {
        this.character = character;
        this.nickName = nickName;
        this.level = level;
        this.setSkill();
    }
}
