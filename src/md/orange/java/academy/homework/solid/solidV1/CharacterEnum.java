package md.orange.java.academy.homework.solid.solidV1;

public enum CharacterEnum {
    ALLYSOLDIER, ALLYDRAGON, ALLYMAGE, ALLYCOMMANDER,ENEMYSOLDIER, ENEMYDRAGON, ENEMYWITCH, ENEMYCOMMANDER
}
