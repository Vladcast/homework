package md.orange.java.academy.homework.solid.solidV1;

public interface SkillDescription {
    void attack();

    void walk();

    void protect();

    void fly();

    void castSpell();

    void promoteSubordinate();

    void sendOrders();

    void curse();
}
