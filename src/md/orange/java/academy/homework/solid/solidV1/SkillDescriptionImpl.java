package md.orange.java.academy.homework.solid.solidV1;

public class SkillDescriptionImpl implements SkillDescription {

    @Override
    public void attack() {
        System.out.println("Attack");

    }

    @Override
    public void walk() {
        System.out.println("Walk");
    }

    @Override
    public void protect() {
        System.out.println("Protect");
    }

    @Override
    public void fly() {
        System.out.println("Fly");
    }

    @Override
    public void castSpell() {
        System.out.println("Cast Spell");
    }

    @Override
    public void promoteSubordinate() {
        System.out.println("Promote Coordinate");
    }

    @Override
    public void sendOrders() {
        System.out.println("Send Orders");
    }

    @Override
    public void curse() {
        System.out.println("Curse");
    }
}
