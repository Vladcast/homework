package md.orange.java.academy.homework.solid.solidV1;

public enum SkillEnumeration {
    ATTACK, WALK, PROTECT, FLY, CASTSPELL, PROMOTESUBORDINATE, SENDORDERS, CURSE
}
