package md.orange.java.academy.homework.solid.solidV1;

import java.util.ArrayList;

public class SkillSetterImpl implements SkillSetter {

    @Override
    public ArrayList<SkillEnumeration> setSkill(Character Character) {
        ArrayList<SkillEnumeration> skillList = new ArrayList<>();
        switch (Character.getRace()) {
            case ALLYSOLDIER: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.WALK);
                skillList.add(SkillEnumeration.PROTECT);
                break;
            }
            case ALLYDRAGON: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.FLY);
                break;
            }
            case ALLYMAGE: {
                skillList.add(SkillEnumeration.CASTSPELL);
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.FLY);
                break;
            }

            case ALLYCOMMANDER: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.PROMOTESUBORDINATE);
                skillList.add(SkillEnumeration.SENDORDERS);
                break;
            }
            case ENEMYDRAGON: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.FLY);
                break;
            }
            case ENEMYSOLDIER: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.WALK);
                break;
            }
            case ENEMYWITCH: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.CURSE);
                skillList.add(SkillEnumeration.FLY);
                break;
            }
            case ENEMYCOMMANDER: {
                skillList.add(SkillEnumeration.ATTACK);
                skillList.add(SkillEnumeration.PROMOTESUBORDINATE);
                skillList.add(SkillEnumeration.SENDORDERS);
                break;
            }
        }
        return skillList;
    }
}
