package md.orange.java.academy.homework.solid.solidV1;

public class SkillView extends SkillDescriptionImpl {
    public void actionView(SkillEnumeration skill) {
        switch (skill) {
            case ATTACK: {
                attack();
                break;
            }
            case WALK: {
                walk();
                break;
            }
            case PROTECT: {
                protect();
                break;
            }
            case FLY: {
                fly();
                break;
            }
            case CASTSPELL: {
                castSpell();
                break;
            }
            case PROMOTESUBORDINATE: {
                promoteSubordinate();
                break;
            }
            case SENDORDERS: {
                sendOrders();
                break;
            }
            case CURSE: {
                curse();
                break;
            }
        }
    }
}
