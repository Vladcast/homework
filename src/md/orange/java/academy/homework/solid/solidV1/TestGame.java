package md.orange.java.academy.homework.solid.solidV1;

import org.junit.Test;

public class TestGame {
    @Test
    public static void main(String[] args) {
        Character allySoldier = new Character(CharacterEnum.ALLYSOLDIER, "Ally", 100L);
        Character allyCommander = new Character(CharacterEnum.ALLYCOMMANDER, "Commander", 50L);
        Character enemyWitch = new Character(CharacterEnum.ENEMYWITCH, "Witch", 99L);
        System.out.println(allySoldier.getSkill());
        System.out.println(allyCommander.getSkill());
        System.out.println(enemyWitch.getSkill());
    }
}
