package md.orange.java.academy.homework.solid.solidV2;

public abstract class Character {
    public void attack() {
        System.out.println(this.getClass().getSimpleName() + " can Attack!");
    }
}
