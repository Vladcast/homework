package md.orange.java.academy.homework.solid.solidV2;

import md.orange.java.academy.homework.solid.solidV2.ally.AllyCommander;

import md.orange.java.academy.homework.solid.solidV2.ally.AllySoldier;
import md.orange.java.academy.homework.solid.solidV2.enemy.EnemyDragon;
import org.junit.Test;

public class TestGame {
    @Test
    public static void main(String[] args) {

        AllyCommander allyCommander = new AllyCommander();

        allyCommander.sendOrders();
        allyCommander.promoteSubordinate();
        allyCommander.attack();


        EnemyDragon enemyDragon = new EnemyDragon();

        enemyDragon.attack();
        enemyDragon.fly();


        AllySoldier allySoldier = new AllySoldier();

        allySoldier.attack();
        allySoldier.protect();
        allySoldier.walk();
    }
}