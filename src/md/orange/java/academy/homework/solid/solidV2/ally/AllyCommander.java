package md.orange.java.academy.homework.solid.solidV2.ally;

import md.orange.java.academy.homework.solid.solidV2.skills.Flyable;
import md.orange.java.academy.homework.solid.solidV2.skills.Commander;

public class AllyCommander extends Ally implements Flyable, Commander {

    @Override
    public void promoteSubordinate() {
        System.out.println(this.getClass().getSimpleName() + " can Promote Subordinate!");

    }

    @Override
    public void sendOrders() {
        System.out.println(this.getClass().getSimpleName() + " can Send Orders!");
    }

    @Override
    public void fly() {
        System.out.println("Fly!");
    }
}
