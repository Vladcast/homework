package md.orange.java.academy.homework.solid.solidV2.ally;

import md.orange.java.academy.homework.solid.solidV2.skills.Flyable;

public class AllyDragon extends Ally implements Flyable {

    public void fly() {
        System.out.println("I can Fly!");
    }
}
