package md.orange.java.academy.homework.solid.solidV2.ally;

import md.orange.java.academy.homework.solid.solidV2.skills.Flyable;

public class AllyMage extends Ally implements Flyable {

    public void castSpell() {
        System.out.println(this.getClass().getSimpleName() + " can Cast Spell");
    }

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " can Fly");
    }
}
