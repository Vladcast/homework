package md.orange.java.academy.homework.solid.solidV2.ally;

import md.orange.java.academy.homework.solid.solidV2.skills.Walkable;

public class AllySoldier extends Ally implements Walkable {

    public void walk() {
        System.out.println(this.getClass().getSimpleName() + " can Walk!");
    }

    public void protect() {
        System.out.println(this.getClass().getSimpleName() + " can Protect!");
    }
}
