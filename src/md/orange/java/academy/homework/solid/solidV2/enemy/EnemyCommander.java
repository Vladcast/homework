package md.orange.java.academy.homework.solid.solidV2.enemy;

import md.orange.java.academy.homework.solid.solidV2.skills.Commander;
import md.orange.java.academy.homework.solid.solidV2.skills.Flyable;

public class EnemyCommander extends Enemy implements Flyable, Commander {

    @Override
    public void promoteSubordinate() {
        System.out.println(this.getClass().getSimpleName() + " can Promote Subordinate!");
    }

    @Override
    public void sendOrders() {
        System.out.println(this.getClass().getSimpleName() + " can Send Orders!");
    }

    @Override
    public void fly() {
        System.out.println(" I can Fly ");
    }
}
