package md.orange.java.academy.homework.solid.solidV2.enemy;

import md.orange.java.academy.homework.solid.solidV2.skills.Flyable;

public class EnemyDragon extends Enemy implements Flyable {

    public void fly() {
        System.out.println(this.getClass().getSimpleName() + " can Fly");
    }
}
