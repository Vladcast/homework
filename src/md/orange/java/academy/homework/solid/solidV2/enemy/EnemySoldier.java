package md.orange.java.academy.homework.solid.solidV2.enemy;

import md.orange.java.academy.homework.solid.solidV2.skills.Walkable;

public class EnemySoldier extends Enemy implements Walkable {
    @Override
    public void walk() {
        System.out.println(this.getClass().getSimpleName() + " can Walk!");
    }
}
