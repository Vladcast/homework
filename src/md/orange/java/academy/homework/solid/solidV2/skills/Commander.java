package md.orange.java.academy.homework.solid.solidV2.skills;

public interface Commander {
    void promoteSubordinate();

    void sendOrders();
}
