package md.orange.java.academy.homework.solid.solidV2.skills;

public interface Flyable {

    void fly();
}
