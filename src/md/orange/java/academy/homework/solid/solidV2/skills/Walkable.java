package md.orange.java.academy.homework.solid.solidV2.skills;

public interface Walkable {
    void walk();
}
